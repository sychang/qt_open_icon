#ifndef SERIALFORM_H
#define SERIALFORM_H

#include <QWidget>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QTimer>

typedef struct{

    quint8  u8DstIpAddr[4];
    quint16 u16MsgLen;
    quint8  u8MsgData[1024];

}STR_SERIAL_SND_DATA;

namespace Ui {
class SerialForm;
}

class SerialForm : public QWidget
{
    Q_OBJECT

public:
    explicit SerialForm(QWidget *parent = 0);
    ~SerialForm();
private slots:
    void handleSerialError(QSerialPort::SerialPortError err);
    void handle_connect_serial();
    void handle_disconnect_serial();

    void handle_send_serial();

    void handle_clear_serial();

    void hand_recv_serial();

    void handle_recv_finsh();

    void on_checkBox_broadcast_stateChanged(int arg1);

private:
    QTimer *ptimer;
    QSerialPort *pserialport;
    Ui::SerialForm *ui;
};

#endif // SERIALFORM_H
