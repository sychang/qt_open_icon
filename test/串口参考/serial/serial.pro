#-------------------------------------------------
#
# Project created by QtCreator 2018-08-31T16:07:03
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = serial
TEMPLATE = app


SOURCES += main.cpp\
        serialwidget.cpp \
    serialform.cpp

HEADERS  += serialwidget.h \
    serialform.h

FORMS    += serialwidget.ui \
    serialform.ui
