﻿/**
 ** @author:      王世雄
 ** @date:	   2019.03.13
 ** @brief:      Aes加密
 ** 使用步骤:
 ** 1.AES加密key支持16、24、32位，分别对应AES_128、AES_192、AES_256
 ** 2.加密模式支持ECB、CBC、CFB、OFB
 ** 3.填充方式支持ZERO、PKCS7、ISO，默认设置是ISO
 ** 4.加密函数：Crypt/encode
 ** 5.解密函数：Decrypt/decode，解密时注意配合使用removePadding函数
 */
#ifndef QAESENCRYPTION_H
#define QAESENCRYPTION_H

#include <QObject>
#include <QByteArray>
#include <QCryptographicHash>

class QAESEncryption : public QObject
{
    Q_OBJECT
public:
    enum Aes {
        AES_128,
        AES_192,
        AES_256
    };

    enum Mode {
        ECB,
        CBC,
        CFB,
        OFB
    };

    enum Padding {
      ZERO,
      PKCS7,
      ISO
    };

    static QByteArray Crypt(QAESEncryption::Aes level, QAESEncryption::Mode mode, const QByteArray &rawText, const QByteArray &key,
                            const QByteArray &iv = NULL, QAESEncryption::Padding padding = QAESEncryption::ISO);
    static QByteArray Decrypt(QAESEncryption::Aes level, QAESEncryption::Mode mode, const QByteArray &rawText, const QByteArray &key,
                              const QByteArray &iv = NULL, QAESEncryption::Padding padding = QAESEncryption::ISO);
    static QByteArray ExpandKey(QAESEncryption::Aes level, QAESEncryption::Mode mode, const QByteArray &key);
    static QByteArray RemovePadding(const QByteArray &rawText, QAESEncryption::Padding padding);

    QAESEncryption(QAESEncryption::Aes level, QAESEncryption::Mode mode,
                   QAESEncryption::Padding padding = QAESEncryption::ISO);

    QByteArray encode(const QByteArray &rawText, const QByteArray &key, const QByteArray &iv = NULL);
    QByteArray decode(const QByteArray &rawText, const QByteArray &key, const QByteArray &iv = NULL);
    QByteArray removePadding(const QByteArray &rawText);
    QByteArray expandKey(const QByteArray &key);

signals:

public slots:

private:
    int m_nb;
    int m_blocklen;
    int m_level;
    int m_mode;
    int m_nk;
    int m_keyLen;
    int m_nr;
    int m_expandedKey;
    int m_padding;
    QByteArray* m_state;

    struct AES256{
        int nk = 8;
        int keylen = 32;
        int nr = 14;
        int expandedKey = 240;
    };

    struct AES192{
        int nk = 6;
        int keylen = 24;
        int nr = 12;
        int expandedKey = 209;
    };

    struct AES128{
        int nk = 4;
        int keylen = 16;
        int nr = 10;
        int expandedKey = 176;
    };

    quint8 getSBoxValue(quint8 num){return sbox[num];}
    quint8 getSBoxInvert(quint8 num){return rsbox[num];}

    void addRoundKey(const quint8 round, const QByteArray expKey);
    void subBytes();
    void shiftRows();
    void mixColumns();
    void invMixColumns();
    void invSubBytes();
    void invShiftRows();
    QByteArray getPadding(int currSize, int alignment);
    QByteArray cipher(const QByteArray &expKey, const QByteArray &plainText);
    QByteArray invCipher(const QByteArray &expKey, const QByteArray &plainText);
    QByteArray byteXor(const QByteArray &in, const QByteArray &iv);

    static quint8 sbox[256];
    static quint8 rsbox[256];
    static quint8 Rcon[256];
};

#endif // QAESENCRYPTION_H
