#ifndef DATABASEWIG_H
#define DATABASEWIG_H

#include <QWidget>
#include "studentdaoimpl.h"

namespace Ui {
class DataBaseWig;
}

class DataBaseWig : public QWidget
{
    Q_OBJECT

public:
    explicit DataBaseWig(QWidget *parent = 0);

private slots:

    void on_addStuBtn_clicked();

    void on_randmStuBtn_clicked();

    void on_delStuBtn_clicked();

    void on_stuTable_clicked(const QModelIndex &index);

private:
    void updateStuTable();
    Ui::DataBaseWig *ui;
    StudentDaoImpl stuDao;
};

#endif // DATABASEWIG_H
