/**
 ** @author:	 Greedysky
 ** @date:       2019.4.15
 ** @brief:      按钮组件
 */
#ifndef CHECKBUTTON_H
#define CHECKBUTTON_H

#include "checkable.h"

class CheckButton : public Checkable
{
    Q_OBJECT
public:
    explicit CheckButton(QWidget *parent = nullptr);

};

#endif
