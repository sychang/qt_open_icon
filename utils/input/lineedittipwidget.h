/*
* Copyright (C) 2019 ~ 2021 Uniontech Software Technology Co.,Ltd.
*
* Author:     wangyou <wangyou@uniontech.com>
*
* Maintainer: wangyou <wangyou@uniontech.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef LINEEDITTIPWIDGET_H
#define LINEEDITTIPWIDGET_H

#include <QLineEdit>
#include <QHBoxLayout>
#include <QLabel>

class LineEditTipWidget : public QLineEdit
{
    Q_OBJECT
public:
    explicit LineEditTipWidget(QWidget *parent = nullptr);
    ~LineEditTipWidget();

protected:
    virtual void paintEvent(QPaintEvent *event) override;
    virtual void focusInEvent(QFocusEvent *event) override;
    virtual void focusOutEvent(QFocusEvent *event) override;


private:

    QHBoxLayout *hlayout;
    QLabel* tipText;
    QString placeholderText;
    QFont m_font;

public:
    void setPlaceholderText(const QString &);

public slots:
    void soltFocusChange(bool);

};

#endif // LINEEDITTIPWIDGET_H
